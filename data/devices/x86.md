---
name: 'Desktop PC'
comment: 'experimental'
deviceType: 'tv'
enableMdRendering: true
image: 'https://unity8.io/img.png'
maturity: .1
buyLink: 'disabled'
---

You can install [Lomiri](https://unity8.io), the desktop environment of Ubuntu Touch previously known as Unity 8, on all debian-derived Linux distributions, including Ubuntu.
